const express = require('express');
const router = express.Router();
const userDAO = require('../services/userDAO');
const {
  createUser,
  usersList,
  getUser,
  updateUser,
  deleteUser,
} = require('../controllers/user');

router.post('/users/create', async (req, res) => {
  console.log('user/create route');
  console.log(req.body);

  try {
    const result = await userDAO.createUser(req.body);
    console.log(result);
    res.status(200).json({ success: true, data: result });
  } catch (error) {
    console.log(error);
    res.status(400).json({ success: false, msg: error.message });
  }
});
//Usage(POST): http://localhost:5000/users/create
//input in Body ->> raw ->> json

router.get('/users/list', async (req, res) => {
  console.log('user list route');
  //console.log(req.body);

  var userList = await userDAO.userList();
  //console.log('returned to list route');
  console.log(userList);
  res.status(200).json({ success: true, data: userList });
});
//Usage: http://localhost:5000/users/list

router.get('/users/:emailId', async (req, res) => {
  console.log('get user route');
  const { emailId } = req.params;
  console.log(emailId);

  var result = await userDAO.findByEmail(emailId);
  //console.log('returned to list route');
  console.log(result);
  res.status(200).json({ success: true, data: result });
});
//Usage: http://localhost:5000/users/kalai.efloz@gmail.com

router.put('/users/:emailId', async (req, res) => {
  console.log('update user route');
  const { emailId } = req.params;
  //const { password } = req.body;
  //console.log(emailId);
  //console.log(password);
  try {
    const result = await userDAO.updatePassword(emailId, password);
    //console.log('returned to update route');
    console.log(result);
    res.status(200).json({ success: true, data: result });
  } catch (error) {
    console.log(error);
    res.status(400).json({ success: false, msg: error.message });
  }
});
//Usage(PUT): http://localhost:5000/users/kalai.efloz@gmail.com
//input in Body ->> raw ->> json

router.delete('/users/:emailId', async (req, res) => {
  console.log('delete user route');

  //console.log(emailId);
  //console.log(password);
  try {
    const result = await userDAO.deleteUser(req.params.emailId);
    //console.log('returned to delete route');
    //console.log(result);
    if (result.deletedCount === 1) {
      console.log('doc deleted');
      res.status(200).json({ success: true, data: result });
    } else {
      res.status(200).json({ success: true, data: 'No doc matched' });
    }
  } catch (error) {
    console.log(error);
    res.status(400).json({ success: false, msg: error.message });
  }
});
//Usage(PUT): http://localhost:5000/users/kalai.efloz@gmail.com

module.exports = router;
