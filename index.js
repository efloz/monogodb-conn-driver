const express = require('express');
const keys = require('./config/keys');
const mongodb = require('mongodb');
const userDAO = require('./services/userDAO');
const authRoutes = require('./routes/userRoutes');

const app = express();

//***especially for post and put req/
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
//********************************* */
app.use('/', authRoutes);
//require('./routes/authRoutes')(app);

mongodb.MongoClient.connect(keys.mongoURI, {
  poolSize: 50,
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .catch((err) => {
    console.error(err.stack);
    process.exit(1);
  })
  .then(async (client) => {
    let user;
    try {
      user = await userDAO.injectDB(client);
    } catch (error) {
      console.error(error);
    }

    //start to listen the app
    const port = process.env.PORT || 5000;
    app.listen(port, () => console.log('App listening on port ' + port));
    //console.log(user);
  });
