const mongodb = require('mongodb');
const keys = require('../config/keys');

let user;
module.exports = class UserDAO {
  static async injectDB(conn) {
    if (user) {
      return;
    }
    try {
      user = await conn.db(keys.mongoDB).collection('users');
      return user;
    } catch (err) {
      console.error(
        `unable to establish a collection handle in userDAO:${err}`
      );
    }
  }
  static findByEmail1(emailId, fn) {
    user.findOne({ email: emailId }, function (err, userRes) {
      if (err) {
        return fn(err, null);
      }
      console.log('find by email');
      console.log(userRes);
      if (!userRes) {
        return fn(null, null);
      }
      return fn(null, userRes);
    });
  }

  static async findByEmail(emailId) {
    try {
      //console.log('Get all Document');

      const result = await user.findOne({ email: emailId });
      return result;
    } catch (error) {
      console.log('Error', error);
      return error;
    }
  }

  static findByID1(id, fn) {
    //console.log('find by id fn');
    //console.log(user);
    var obj_id = mongodb.ObjectId(id);
    user.findOne({ _id: obj_id }, function (err, userRes) {
      //console.log(userRes);
      if (err) {
        return fn(err, null);
      }
      if (!userRes) {
        return fn(null, null);
      }
      return fn(null, userRes);
    });
  }

  static async findByID(id) {
    //console.log('find by id fn');
    //console.log(user);
    try {
      //console.log('Get all Document');
      var obj_id = mongodb.ObjectId(id);
      const result = await user.findOne({ _id: obj_id }).toArray();
      return result;
    } catch (error) {
      console.log('Error', error);
      return error;
    }
  }

  static async createUser(doc) {
    console.log('create user called');

    try {
      const result = await user.insertOne(doc);
      return result.ops[0];
    } catch (error) {
      return fn(error, null);
    }
  }

  static async userList() {
    // console.log('user List called');
    // var usersList = await user.find().toArray();
    // console.log(usersList);
    // return usersList;
    try {
      //console.log('Get all Document');

      const result = await user.find({}).toArray();
      return result;
    } catch (error) {
      console.log('Error', error);
      return error;
    }
  }

  static async updatePassword(mailId, pwd) {
    try {
      const updateDoc = { $set: { password: pwd } };
      const result = await user.updateOne({ email: mailId }, updateDoc);
      //console.log(result);
      return result;
    } catch (error) {
      //console.log(error);
      throw error;
    }
  }

  static async deleteUser(mailId) {
    try {
      //const updateDoc = { $set: { password: pwd } };
      const result = await user.deleteOne({ email: mailId });
      //console.log(result);
      return result;
    } catch (error) {
      //console.log(error);
      throw error;
    }
  }
};
