const userDAO = require('../services/userDAO');

const createUser = (req, res) => {
  console.log('user/create route');
  console.log(req.body);

  userDAO.createUser(
    req.body.emailid,
    req.body.password,
    function (err, result) {
      if (err) {
        console.log('error');
        res.send(err);
      }
      console.log(result);
      res.status(201).json({ succcess: true, user: result });
      //res.send(result);
    }
  );
};

async function usersList(req, res) {
  console.log('user list route');
  //console.log(req.body);

  var userList = await userDAO.userList();
  console.log('returned to list route');
  console.log(userList);
  res.status(200).json({ success: true, data: userList });
}

async function getUser(req, res) {
  console.log('get user route');
  const { emailId } = req.params;
  console.log(emailId);

  userDAO.findByEmail(emailId, function (err, result) {
    if (err) {
      console.log('error');
      res.send(err);
    }
    console.log(result);
    res.status(201).json({ succcess: true, user: result });
    //res.send(result);
  });
  // console.log('returned to list route');
  // console.log(user);
  // res.status(200).json({ success: true, data: user });
}

async function updateUser(req, res) {
  console.log('update user route');
  const { emailId } = req.params;
  const { password } = req.body;
  //console.log(emailId);
  //console.log(password);
  try {
    const result = await userDAO.updatePassword(emailId, password);
    console.log('returned to update route');
    //console.log(result);
    res.status(200).json({ success: true, data: result });
  } catch (error) {
    console.log(error);
    res.status(400).json({ success: false, msg: error.message });
  }
}

async function deleteUser(req, res) {
  console.log('delete user route');

  //console.log(emailId);
  //console.log(password);
  try {
    const result = await userDAO.deleteUser(req.params.emailId);
    //console.log('returned to delete route');
    //console.log(result);
    if (result.deletedCount === 1) {
      console.log('doc deleted');
      res.status(200).json({ success: true, data: result });
    } else {
      res.status(200).json({ success: true, data: 'No doc matched' });
    }
  } catch (error) {
    console.log(error);
    res.status(400).json({ success: false, msg: error.message });
  }
}

module.exports = {
  createUser,
  usersList,
  getUser,
  updateUser,
  deleteUser,
};
